const TokenRefresh = (function() {
  let timer;

  function refreshToken() {
    fetch('/refreshToken')
      .then(response => {
        if(response.status !== 200) {
          throw new Error(response.status)
        }
        return response.json();
      }).then(result => {
        if (!result.status)
          throw new Error(result)
      }).catch(error => {
        console.error('Error:', error);
        clearInterval(timer);
        timer = setInterval(TokenRefresh.watchToken, 5000);
      });
  }

  function getToken() {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; token=`);
    if (parts.length === 2)
      return parts.pop().split(';').shift();
  }

  function getDecodedToken() {
    const token = getToken();
    if (!token)
      return;
    const base64Url = getToken().split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
  }

  function isAuthenticated() {
    if (token = getDecodedToken()) {
      let ts = Math.round((new Date()).getTime() / 1000);
      if (token.exp < ts)
        return false;
      return true;
    }
    return false;
  }

  function waitForToken() {
    if (isAuthenticated()) {
      clearInterval(timer);
      timer = setInterval(refreshToken, 30000);
      return;
    }
    if (timer)
      return;
    timer = setInterval(TokenRefresh.watchToken, 5000);
  }

  // publiczna część singletonu - API modułu
  return {
    watchToken: function() {
      waitForToken();
    }
  };
})();

(function () {
  TokenRefresh.watchToken();

  window.addEventListener('load', (e) => {
    const logoutTarget = document.getElementById('logoutButton');
    if (logoutTarget) {
      let orgHtml;
      logoutTarget.addEventListener('mouseenter', e => {
        orgHtml = logoutTarget.innerHTML;
        logoutTarget.innerHTML = '<span class="material-icons">logout</span> <span class="hideable">log out</span>';
      });
      logoutTarget.addEventListener('mouseleave', e => {
        logoutTarget.innerHTML = orgHtml;
      });
    }
  });

})();
