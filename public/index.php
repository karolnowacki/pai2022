<?php

require __DIR__ . '/../vendor/autoload.php';

use PDNSAdmin\Factory\AppFactory;
use PDNSAdmin\Middleware\AuthMiddleware;
use PDNSAdmin\Middleware\BodyParseMiddleware;
use UMA\DIC\Container;
// Prościej kontenera napisać się nie da,
//więc pozwoliłem sobie nie kopiować tych kilkunastu linijek

$container = new Container(require(__DIR__ . '/../config.php'));
$app = AppFactory::create($container);

require __DIR__ . '/../services.php';
require __DIR__ . '/../routing.php';

$app->add(new AuthMiddleware($container, [
  'allow' => [ '/login' ],
  'redirect' => '/login'
]));

$app->add(new BodyParseMiddleware($container));

$app->run();
