<?php

declare(strict_types=1);

use PDNSAdmin\View;
use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Controllers\ControllerProvider;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Controllers\Controllers;
use PDNSAdmin\Services\AuthService;

/* EntityManager Service - this is my way to DB */
$container->set(EntityManager::class, static function (ContainerInterface $c): EntityManager {
    $settings = $c->get('settings');
    return new EntityManager(
        $settings['database']['dsn'],
        $settings['database']['username'] ?? NULL,
        $settings['database']['password'] ?? NULL,
        $settings['database']['options'] ?? NULL
    );
});

/* View as a service */
$container->set(View::class, static function (ContainerInterface $c): View {
    $settings = $c->get('settings');
    return new View($settings['view']['path'], $settings['view']['layout']);
});

/* AuthService */
$container->set(AuthService::class, static function (ContainerInterface $c): AuthService {
    $settings = $c->get('settings');
    return new AuthService($settings['auth']['secret']);
});

/* Register all controllers as services */
$container->register(new Controllers());
