# PowerDNS Administration Panel

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)

## General Information
- UI admin interface for PowerDNS mysql backend schema

## Technologies Used
- Docker
- PHP

## Features
List the ready features here:
- domains list
- domain edit
- records edit
- user management

## Screenshots
- TODO

## Setup
- TODO

## Usage
- TODO

## Project Status
Project is: _in progress_

## Room for Improvement
Everything

## Acknowledgements
Give credit here.
-

## Contact
Created by Karol Nowacki
