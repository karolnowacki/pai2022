<?php

use PDNSAdmin\Controllers\HomeController;
use PDNSAdmin\Controllers\LoginController;
use PDNSAdmin\Controllers\DomainController;
use PDNSAdmin\Controllers\RecordController;
use PDNSAdmin\Controllers\UsersController;

$app->get("/", [ HomeController::class , 'index' ]);
$app->get("/login", [ LoginController::class , 'login' ]);
$app->post("/login", [ LoginController::class , 'login_post' ]);
$app->get("/refreshToken", [ LoginController::class , 'refreshToken' ]);
$app->get("/logout", [ LoginController::class , 'logout' ]);

$app->get("/domain", [ DomainController::class , 'list' ]);
$app->get("/domain/add", [ DomainController::class , 'add' ]);
$app->post("/domain/add", [ DomainController::class , 'add_post' ]);
$app->get("/domain/edit/{id}", [ DomainController::class , 'edit' ]);
$app->post("/domain/edit/{id}", [ DomainController::class , 'edit_post' ]);

$app->get("/record/add/{id}", [ RecordController::class , 'add' ]);
$app->post("/record/add/{id}", [ RecordController::class , 'add_post' ]);
$app->get("/record/edit/{id}", [ RecordController::class , 'edit' ]);
$app->post("/record/edit/{id}", [ RecordController::class , 'edit_post' ]);

$app->get("/users", [ UsersController::class , 'list' ]);
$app->get("/users/add", [ UsersController::class , 'add' ]);
$app->post("/users/add", [ UsersController::class , 'add_post' ]);
$app->get("/users/edit/{id}", [ UsersController::class , 'edit' ]);
$app->post("/users/edit/{id}", [ UsersController::class , 'edit_post' ]);
