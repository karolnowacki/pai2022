<?php

define('APP_ROOT', __DIR__);
if (isset($_SERVER['HTTP_HOST']))
  define('BASE_URL', "http://" . $_SERVER['HTTP_HOST']);

// BE AWARE! This can be changed be CLI tools!

return [
  'settings' => [
    'allow-cli-config-change' => true,
    'view' => [
      'path' => APP_ROOT . '/views/',
      'layout' => 'layout.phtml'
    ],
    'database' => [
      'dsn' => 'sqlite:' . APP_ROOT . '/database.db',
      'username' => null,
      'password' => null
    ],
    'auth' => [
      'secret' => 'ThisIsSuperSecretForJWTTokenPleaseChange'
    ],
    'dns' => [
      'primary' => 'ns.primaryserver.com',
      'hostmaster' => 'admin@example.com',
      'refresh' => 86400,
      'retry' => 7200,
      'expire' => 3600000,
      'minimum' => 3600,
      'ttl' => 3600
    ]
  ]
];
