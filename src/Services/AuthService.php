<?php

declare(strict_types=1);

namespace PDNSAdmin\Services;

//Bo Sesja to zło!!!
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Model\User;

class AuthService {

  private ContainerInterface $container;
  private $secret;
  public const ALGORITHM = 'HS256';
  public const TTL = 600;

  public function __construct(string $secret) {
    $this->secret = $secret;
  }

  public function validateToken($token) {
    try {
      if (!($decoded = JWT::decode($token, new Key($this->secret, self::ALGORITHM))))
        return false;
    } catch (\Throwable $e) {
      return false;
    }
    return $decoded;
  }

  public function createToken(array $payload = []) {
    $payload += [
      'iat' => time(),
      'nbf' => time(),
      "exp" => time()+self::TTL,
    ];
    return JWT::encode($payload, $this->secret, self::ALGORITHM);
  }

}
