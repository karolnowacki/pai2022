<?php

declare(strict_types=1);

namespace PDNSAdmin\Services;

use \PDO;
use Exception;
use PDNSAdmin\Interfaces\EntityInterface;

class EntityManager {

  protected PDO $db;

  public function __construct(string $dsn, ?string $username = null,
    ?string $password = null, ?array $options = null) {
      if (!isset($options)) $options = [];
      $options[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;

      $this->db = new PDO($dsn, $username, $password, $options);
    }

  public function db() {
    return $this->db;
  }

  public function persist(EntityInterface $entity) {
    $data = $entity->toArray();

    $data = $this->fixBooleans($data);

    if ($entity->isNew()) {
      unset($data[$entity->getIdField()]);
      $sql = 'INSERT INTO '.$entity->getTable().' ( '.implode(', ', array_keys($data)).' ) VALUES ( :'.implode(', :', array_keys($data)).')';
      $stmt = $this->db->prepare($sql);
      $stmt->execute($data);
      $entity->set($entity->getIdField(), (int)($this->db->lastInsertId()));
      $entity->setEntityManager($this);
    } else {
      $id = $data[$entity->getIdField()];
      unset($data[$entity->getIdField()]);
      $sql = 'UPDATE '.$entity->getTable().' SET '.implode(', ', array_map(function($key) {
        return $key . ' = :'. $key;
      }, array_keys($data))).' WHERE '.$entity->getIdField().' = :'.$entity->getIdField();
      $stmt = $this->db->prepare($sql);
      $stmt->execute($this->fixBooleans($entity->toArray()));
      $entity->setEntityManager($this);
    }

    //TODO: to można zrobić ładniej
    $entity->setChangedStatus(false);

  }

  public function findOne($class, $value, $field = null) {
    $obj = new $class();
    if (!$field)
      $field = $obj->getIdField();
    if (!in_array($field, $obj->getFields()))
      throw \Exception("Field $field not exists");

    $stmt = $this->db->prepare("SELECT * FROM " .$obj->getTable()." WHERE ".$field." = :value");
    $stmt->execute([ 'value' => $value ]);
    return $stmt->fetchObject($class, [ $this ]);
  }

  public function findAll($class, $value, $field) {
    $obj = new $class();
    if (!in_array($field, $obj->getFields()))
      throw \Exception("Field $field not exists");

    $stmt = $this->db->prepare("SELECT * FROM " .$obj->getTable()." WHERE ".$field." = :value");
    $stmt->execute([ 'value' => $value ]);
    return $stmt->fetchAll(PDO::FETCH_CLASS, $class, [ $this ]);
  }

  public function fetchAll($class) {
    $obj = new $class();
    $stmt = $this->db->prepare("SELECT * FROM " .$obj->getTable());
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, $class, [ $this ]);
  }

  protected function fixBooleans(array $data) {
    //beacuse PDO_PGSQL is stupid
    foreach (array_keys($data) as $key)
      if (is_bool($data[$key])) {
        $data[$key] = $data[$key] ? 'true' : 'false';
      }
    return $data;
  }

}
