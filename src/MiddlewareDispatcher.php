<?php

declare(strict_types=1);

namespace PDNSAdmin;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;

class MiddlewareDispatcher {

  protected RequestHandlerInterface $stack;

  public function __construct( RequestHandlerInterface $base) {
    $this->stack = $base;
  }

  public function handle(ServerRequestInterface $request): ResponseInterface {
    return $this->stack->handle($request);
  }

  public function add(MiddlewareInterface $middleware) {
    $next = $this->stack;
    $this->stack = new class ($middleware, $next) implements RequestHandlerInterface {
      private $middleware;
      private $next;
      public function __construct(MiddlewareInterface $middleware, RequestHandlerInterface $next) {
        $this->middleware = $middleware;
        $this->next = $next;
      }
      public function handle(ServerRequestInterface $request): ResponseInterface {
        return $this->middleware->process($request, $this->next);
      }
      public function __invoke(ServerRequestInterface $request) : ResponseInterface {
        return $this->handle($request);
      }
    };
    return $this;
  }

}
