<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\View;
use PDNSAdmin\Model\User;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Model\Domain;
use PDNSAdmin\Model\Records;

class DomainController extends Controller {

  private $view;

  public function __construct(ContainerInterface $container) {
    parent::__construct($container);

    $this->view = $this->container->get(View::class);
    $this->view->setAttribute('title', "Domain");
    $this->view->setAttribute('user', $this->container->get('user'));
    $this->view->setActive('domain');

  }

  public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $domains = $this->container->get(EntityManager::class)->fetchAll(Domain::class);
    return $this->view->render($response, 'domain_list.phtml', [
      'domains' => $domains
    ]);
  }

  public function add(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    return $this->view->render($response, 'domain_add.phtml', [ 'types' => Domain::TYPES ]);
  }

  public function add_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $data = $request->getParsedBody();
    $domain = new Domain();

    $errors = [];

    try {
      $domain->setName($data['name']);
    } catch (\Throwable $e) {
      unset($data['name']);
      $errors['name'] = $e->getMessage();
    }

    try {
      $domain->setType($data['type']);
    } catch (\Throwable $e) {
      unset($data['type']);
      $errors['type'] = $e->getMessage();
    }

    if (!empty($errors)) {
      $this->view->setAttribute('values', $data);
      $this->view->setAttribute('errors', $errors);
      return $this->add($request, $response, $args);
    }

    $this->container->get(EntityManager::class)->persist($domain);

    $settings = $this->container->get('settings');

    $soa = new Records\SOA();
    $soa->setContent(implode(' ', [
      $settings['dns']['primary'],
       str_replace('@', '.', $settings['dns']['hostmaster']),
      0,
      $settings['dns']['refresh'],
      $settings['dns']['retry'],
      $settings['dns']['expire'],
      $settings['dns']['minimum']
    ]));
    $soa->updateSerial();
    $domain->addRecord($soa);
    $soa->setName($domain->getName());
    $this->container->get(EntityManager::class)->persist($soa);

    return $response->withStatus(302)->withHeader('Location', '/domain/edit/'.$domain->getId());
  }

  public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $domain = $this->container->get(EntityManager::class)->findOne(Domain::class, $args['id']);
    return $this->view->render($response, 'domain_edit.phtml', [
      'types' => Domain::TYPES,
      'domain' => $domain,
      'SOA' => $domain->getSOARecord(),
      'records' => $domain->getRecords()
    ]);
  }

  public function edit_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $domain = $this->container->get(EntityManager::class)->findOne(Domain::class, $args['id']);
    $data = $request->getParsedBody();

    try { $domain->setType($data['type']); } catch (\Throwable $e) { $errors['type'] = $e->getMessage(); }

    $soa = $domain->getSOARecord();

    $errors = [];

    try { $soa->setPrimary($data['SOA_primary']); } catch (\Throwable $e) { $errors['SOA_primary'] = $e->getMessage(); }
    try { $soa->setHostmaster($data['SOA_hostmaster']); } catch (\Throwable $e) { $errors['SOA_hostmaster'] = $e->getMessage(); }
    try { $soa->setSerial((int)$data['SOA_serial']); } catch (\Throwable $e) { $errors['SOA_serial'] = $e->getMessage(); }
    try { $soa->setRefresh((int)$data['SOA_refresh']); } catch (\Throwable $e) { $errors['SOA_refresh'] = $e->getMessage(); }
    try { $soa->setRetry((int)$data['SOA_retry']); } catch (\Throwable $e) { $errors['SOA_retry'] = $e->getMessage(); }
    try { $soa->setExpire((int)$data['SOA_expire']); } catch (\Throwable $e) { $errors['SOA_expire'] = $e->getMessage(); }
    try { $soa->setMinimum((int)$data['SOA_minimum']); } catch (\Throwable $e) { $errors['SOA_minimum'] = $e->getMessage(); }

    if (!empty($errors)) {
      $this->view->setAttribute('errors', $errors);
      return $this->edit($request, $response, $args);
    }

    $domain->save();

    $this->view->addNotification('Domain saved', 'success');

    return $this->edit($request, $response, $args);
  }

}
