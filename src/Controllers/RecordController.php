<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use PDNSAdmin\Model\Record;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\View;
use PDNSAdmin\Model\User;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Model\Domain;

class RecordController extends Controller {

  public function __construct(ContainerInterface $container) {
    parent::__construct($container);

    $this->view = $this->container->get(View::class);
    $this->view->setAttribute('title', "Record");
    $this->view->setAttribute('user', $this->container->get('user'));
    $this->view->setActive('domain');

  }

  public function add(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $domain = $this->container->get(EntityManager::class)->findOne(Domain::class, $args['id']);

    $record = new Record($this->container->get(EntityManager::class));
    $record->setDomain_id($domain->getId());
    $record->setName($domain->getName());

    return $this->view->render($response, 'record_edit.phtml', [
      'types' => Record::TYPES,
      'record' => $record,
      'domain' => $domain
    ]);
  }

  public function add_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $domain = $this->container->get(EntityManager::class)->findOne(Domain::class, $args['id']);
    $data = $request->getParsedBody();
    //Stupid way to ensure domain_id before name for validation :)
    ksort($data);
    $record = new Record($this->container->get(EntityManager::class));
    $errors = $this->bindParams($record, $data);
    if (empty($errors)) {
      $record->save();
      return $response->withStatus(302)->withHeader('Location', '/domain/edit/'.$record->getDomain()->getId());
    }
    $this->view->setAttribute('errors', $errors);
    return $this->add($request, $response, $args);

  }

  public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $record = $this->container->get(EntityManager::class)->findOne(Record::class, $args['id']);
    return $this->view->render($response, 'record_edit.phtml', [
      'types' => Record::TYPES,
      'record' => $record,
      'domain' => $record->getDomain()
    ]);
  }

  public function edit_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $record = $this->container->get(EntityManager::class)->findOne(Record::class, $args['id']);
    $data = $request->getParsedBody();
    ksort($data);
    $errors = $this->bindParams($record, $data);
    if (empty($errors)) {
      $record->save();
      $this->view->addNotification('Domain saved', 'success');
    } else {
      $this->view->setAttribute('errors', $errors);
    }
    return $this->view->render($response, 'record_edit.phtml', [
      'types' => Record::TYPES,
      'record' => $record,
      'domain' => $record->getDomain()
    ]);
  }

  private function bindParams(Record $record, array $params) {
    $errors = [];
    $params['disabled'] = isset($params['disabled']) ? true : false;
    $params['auth'] = isset($params['auth']) ? true : false;
    foreach ($params as $key => $value) {
      if ($value === '') $value = null;
      elseif (in_array($key, ['ttl', 'prio', 'domain_id'])) $value = (int)$value;
      try {
        $record->set($key, $value);
      } catch (\Throwable $e) {
        $errors[$key] = $e->getMessage();
      }
    }
    return $errors;
  }


}
