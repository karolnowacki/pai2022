<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\View;
use PDNSAdmin\Model\User;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\EntityManager;

class UsersController extends Controller {

  public function __construct(ContainerInterface $container) {
    parent::__construct($container);

    $this->view = $this->container->get(View::class);
    $this->view->setAttribute('title', "Users");
    $this->view->setAttribute('user', $this->container->get('user'));
    $this->view->setActive('users');

  }

  public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $users = $this->container->get(EntityManager::class)->fetchAll(User::class);
    return $this->view->render($response, 'users_list.phtml', [
      'users' => $users
    ]);
  }

  public function add(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $user = new User($this->container->get(EntityManager::class));
    return $this->view->render($response, 'users_edit.phtml', [
      'record' => $user
    ]);
  }

  public function add_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $data = $request->getParsedBody();
    $record = new User($this->container->get(EntityManager::class));
    $errors = $this->bindParams($record, $data);
    if (empty($errors)) {
      $record->save();
      return $response->withStatus(302)->withHeader('Location', '/users');
    }
    $this->view->setAttribute('errors', $errors);
    return $this->view->render($response, 'users_edit.phtml', [
      'record' => $user
    ]);
  }

  public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $record = $this->container->get(EntityManager::class)->findOne(User::class, $args['id']);
    return $this->view->render($response, 'users_edit.phtml', [
      'record' => $record
    ]);
  }

  public function edit_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $record = $this->container->get(EntityManager::class)->findOne(User::class, $args['id']);
    $data = $request->getParsedBody();
    $errors = $this->bindParams($record, $data);
    if (empty($errors)) {
      $record->save();
      $this->view->addNotification('User saved', 'success');
    } else {
      $this->view->setAttribute('errors', $errors);
    }
    return $this->view->render($response, 'users_edit.phtml', [
      'record' => $record
    ]);
  }

  private function bindParams(User $record, array $params) {
    $errors = [];

    //TODO: refactor bo to jakaś massakra
    if (array_key_exists('password', $params) && empty($params['password']))
      unset($params['password']);
    if (isset($params['password'])) {
      $record->setPassword($params['password']);
      unset($params['password']);
    }

    $params['is_admin'] = isset($params['is_admin']) ? true : false;
    foreach ($params as $key => $value) {
      if ($value === '') $value = null;
      elseif (in_array($key, ['id'])) $value = (int)$value;
      try {
        $record->set($key, $value);
      } catch (\Throwable $e) {
        $errors[$key] = $e->getMessage();
      }
    }
    return $errors;
  }


}
