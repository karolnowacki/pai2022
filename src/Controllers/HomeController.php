<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\View;
use PDNSAdmin\Model\User;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\EntityManager;

class HomeController extends Controller {

  public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $view = $this->container->get(View::class);
    $em = $this->container->get(EntityManager::class);
    $config = $this->container->get('settings');
    $view->setAttribute('user', $this->container->get('user'));
    return $view->render($response, 'info.phtml', [ 'info' => [
        'PHP Version' => phpversion(),
        'Database engine' => $em->db()->getAttribute(\PDO::ATTR_DRIVER_NAME),
        'DB DSN' => $config['database']['dsn'],
        'View path' => $config['view']['path'],
        'View layout' => $config['view']['layout']
      ]
    ]);
  }
}
