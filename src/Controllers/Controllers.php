<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use UMA\DIC\ServiceProvider;
use Psr\Container\ContainerInterface;
use UMA\DIC\Container;

class Controllers implements ServiceProvider{

  public function provide(ContainerInterface $c) : void {
    foreach (glob(__DIR__ . "/*Controller.php") as $filename) {
      $filename = basename($filename, '.php');
      if ($filename == 'Controller') continue;
      $class = 'PDNSAdmin\\Controllers\\'.$filename;
      $c->set($class, static function(ContainerInterface $c) use ($class) { return new $class($c); });
    }
  }

 }
