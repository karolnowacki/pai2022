<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\View;
use PDNSAdmin\Model\User;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Services\AuthService;
use PDNSAdmin\Services\EntityManager;

class LoginController extends Controller {

  public function login(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $view = $this->container->get(View::class);
    $view->setAttribute('title', "Login");
    return $view->render($response, 'login.phtml');
  }

  public function login_post(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $view = $this->container->get(View::class);
    $data = $request->getParsedBody();

    if ($user = $this->container->get(EntityManager::class)->findOne(User::class, $data['login'], 'username'))
        if ($user->verifyPassword($data['password'])) {
          $token = $this->container->get(AuthService::class)->createToken(['username' => $user->getUsername() ]);
          return $response->withCookieParams(['token' => $token ])
            ->withProtocolVersion('1.1')->withStatus(302)->withHeader('Location', '/');
        }

    $view->setAttribute('title', "Login");
    return $view->render($response, 'login.phtml', [ 'error' => 'Login Invalid' ]);
  }

  public function refreshToken(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    if ($user = $this->container->get('user')) {
        $token = $this->container->get(AuthService::class)->createToken(['username' => $user->getUsername() ]);
        return $response->withCookieParams(['token' => $token ])->withJson(['status' => true]);
    }
    return $response->withStatus(401);
  }

  public function logout(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    return $response->withCookieParams(['token' => '' ])
      ->withProtocolVersion('1.1')->withStatus(302)->withHeader('Location', '/');
  }

}
