<?php

declare(strict_types=1);

namespace PDNSAdmin\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Container\ContainerInterface;

abstract class Controller {

   protected $container;

   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }

 }
