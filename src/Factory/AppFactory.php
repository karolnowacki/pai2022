<?php

declare(strict_types=1);

namespace PDNSAdmin\Factory;

use PDNSAdmin\App;
use Psr\Container\ContainerInterface;

class AppFactory {

  public static function create(?ContainerInterface $container = null) {
    return new App($container);
  }

}
