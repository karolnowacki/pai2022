<?php

declare(strict_types=1);

namespace PDNSAdmin\Interfaces;

interface EntityInterface {

  function getTable() : string;
  function getIdField() : string;
  public function isNew() : bool;
  public function toArray() : array;

}
