<?php

declare(strict_types=1);

namespace PDNSAdmin\Model;

class Record extends Entity {

  protected int $id;
  protected ?int $domain_id;
  protected ?string $name;
  protected ?string $type;
  protected ?string $content;
  protected ?int $ttl;
  protected ?int $prio;
  protected bool $disabled = false;
  protected ?string $ordername;
  protected bool $auth = false;

  private $domain;

  public const TYPES = [ 'A', 'AAAA', 'AFSDB', 'ALIAS', 'APL', 'CAA', 'CERT', 'CDNSKEY', 'CDS', 'CNAME', 'CSYNC',
      'DNSKEY', 'DNAME', 'DS', 'HINFO', 'HTTPS', 'KEY', 'LOC', 'MX', 'NAPTR', 'NS', 'NSEC', 'NSEC3', 'NSEC3PARAM',
      'OPENPGPKEY', 'PTR', 'RP', 'RRSIG', 'SOA', 'SPF', 'SSHFP', 'SRV', 'SVCB', 'HTTPS', 'TKEY', 'TSIG', 'TLSA',
      'SMIMEA', 'TXT', 'URI' ];

  public function getTable() : string { return 'records'; }
  public function getIdField() : string { return 'id'; }

  public function validateType($type) {
    return in_array($type, self::TYPES);
  }

  public function validateName($name) {
    if (empty($name))
      throw new \InvalidArgumentException("name is required");
    if ($domain = $this->getDomain()) {
      if (isset($domain) && substr($name, -strlen($domain->getName())) !== $domain->getName())
        throw new \InvalidArgumentException("name must ends with domain name");
    }

    return preg_match('/\b((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b/', $name);
  }

  public function castAs($newClass) {
    $obj = new $newClass;
    if (!($obj instanceof self))
      throw InvalidArgumentException("$newClass not inherits from ".__CLASS__);

    foreach (get_object_vars($this) as $key => $name) {
        $obj->$key = $name;
    }
    return $obj;
  }

  public function isDisabled() {
    return $this->disabled;
  }

  public function isAuth() {
    return $this->auth;
  }

  public function getDomain() : ?Domain {
    if ($this->domain)
      return $this->domain;
    if (!isset($this->domain_id, $this->_em))
      return null;
    return $this->domain = $this->_em->findOne(Domain::class, $this->domain_id);
  }

}
