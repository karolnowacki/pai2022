<?php

declare(strict_types=1);

namespace PDNSAdmin\Model\Records;

use PDNSAdmin\Model\Record;

class SOA extends Record {

  public const DESCRIPTION = <<<EOD
<p>The Start of Authority record is one of the most complex available. It
specifies a lot about a domain: the name of the master nameserver (‘the
primary’), the hostmaster and a set of numbers indicating how the data
in this domain expires and how often it needs to be checked. Further
more, it contains a serial number which should rise on each change of
the domain.</p>
<pre>primary hostmaster serial refresh retry expire minimum</pre>
<p>Besides the primary and the hostmaster, all fields are numerical.
The fields have complicated and sometimes controversial meanings.</p>
EOD;

  private string $primary;
  private string $hostmaster;
  private int $serial;
  private int $refresh;
  private int $retry;
  private int $expire;
  private int $minimum;

  public function __construct() {
    parent::__construct(...func_get_args());
    $this->setType('SOA');
  }

  public function getPrimary() { return isset($this->primary) ? $this->primary : $this->primary = explode(' ', $this->getContent())[0]; }
  public function getHostmaster() { return isset($this->hostmaster) ? $this->hostmaster : $this->hostmaster = preg_replace('/\./', '@', explode(' ', $this->getContent())[1], 1); }
  public function getSerial() { return isset($this->serial) ? $this->serial : $this->serial = (int)explode(' ', $this->getContent())[2]; }
  public function getRefresh() { return isset($this->refresh) ? $this->refresh : $this->refresh = (int)explode(' ', $this->getContent())[3]; }
  public function getRetry() { return isset($this->retry) ? $this->retry : $this->retry = (int)explode(' ', $this->getContent())[4]; }
  public function getExpire() { return isset($this->expire) ? $this->expire : $this->expire = (int)explode(' ', $this->getContent())[5]; }
  public function getMinimum() { return isset($this->minimum) ? $this->minimum : $this->minimum = (int)explode(' ', $this->getContent())[6]; }

  public function setPrimary(string $primary) { $this->primary = $primary; $this->updateContent(); }
  public function setHostmaster(string $hostmaster) { $this->hostmaster = str_replace('@', '.', $hostmaster); $this->updateContent(); }
  public function setSerial(int $serial) { $this->serial = $serial; $this->updateContent(); }
  public function setRefresh(int $refresh) { $this->refresh = $refresh; $this->updateContent();}
  public function setRetry(int $retry) { $this->retry = $retry; $this->updateContent(); }
  public function setExpire(int $expire) { $this->expire = $expire; $this->updateContent(); }
  public function setMinimum(int $minimum) { $this->minimum = $minimum; $this->updateContent(); }

  private function updateContent() {
    $this->setContent(implode(' ', [
      $this->getPrimary(), $this->getHostmaster(), $this->getSerial(),
      $this->getRefresh(), $this->getRetry(), $this->getExpire(), $this->getMinimum()
    ]));
  }

  public function updateSerial() {
    $serial = (int)(date("Ymd")."01");
    if ($serial < ($this->getSerial() ?? 0))
      $serial = $this->getSerial()+1;
    $this->setSerial($serial);
    $this->updateContent();
  }

  public function getContent() {
    $value = parent::getContent();
    if (!isset($value))
      $value = '      ';
    if (strlen($value) < 6)
      $value = $value .= '      ';
    return $value;
  }


}
