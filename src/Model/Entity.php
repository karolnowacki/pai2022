<?php

declare(strict_types=1);

namespace PDNSAdmin\Model;

use PDNSAdmin\Interfaces\EntityInterface;
use PDNSAdmin\Services\EntityManager;

abstract class Entity implements EntityInterface {

  abstract function getTable() : string;
  abstract function getIdField() : string;

  protected EntityManager $_em;
  protected bool $_hasChanged = false;

  public function __construct(?EntityManager $em = null) {
    if ($em) $this->_em = $em;
  }

  public function isNew() : bool {
    return !isset($this->{$this->getIdField()});
  }

  public function toArray() : array {
    return array_filter(get_object_vars($this), function ($key) {
      return $key[0] !== '_';
    }, \ARRAY_FILTER_USE_KEY);
  }

  public function getFields() : array {
    return array_keys(array_filter(get_class_vars(get_class($this)), function ($key) {
      return $key[0] !== '_';
    }, \ARRAY_FILTER_USE_KEY));
  }

  public function __call($name, $args) {
    if (strpos($name, 'get') === 0) {
      return $this->get(lcfirst(substr($name, 3)));
    }
    if (strpos($name, 'set') === 0) {
      return $this->set(lcfirst(substr($name, 3)), $args[0]);
    }
    throw new \BadMethodCallException();
  }

  public function get($field) {
    if (in_array($field, $this->getFields()))
      if (isset($this->$field))
        return $this->$field;
      return null;
    throw new \InvalidArgumentException("Field $field not exists");
  }

  public function set($field, $value) {
    if (!in_array($field, $this->getFields()))
      throw new \InvalidArgumentException("Field $field not exists");

    $validateMethod = 'validate'.ucfirst($field);
    if (method_exists($this, $validateMethod))
      if (!$this->$validateMethod($value))
        throw new \InvalidArgumentException("Value $value is invalid for $field");
    if (isset($this->$field) && $this->$field !== $value)
      $this->_hasChanged = true;
    return $this->$field = $value;

  }

  public function setEntityManager(EntityManager $em) {
    $this->_em = $em;
  }

  public function save() {
    if ($this->_hasChanged || $this->isNew())
      $this->_em->persist($this);
  }

  //TODO: Nie podoba mi się :(
  public function setChangedStatus(bool $status) {
    $this->_hasChanged = $status;
  }

}
