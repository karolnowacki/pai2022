<?php

declare(strict_types=1);

namespace PDNSAdmin\Model;

use PDNSAdmin\Model\Records\SOA;

class Domain extends Entity {

  protected int $id;
  protected string $name;
  protected ?string $master;
  protected ?int $last_check;
  protected string $type;
  protected ?int $notified_serial;
  protected ?string $account;

  private array $records;
  private SOA $soa;

  public const TYPES = [ 'NATIVE', 'MASTER', 'SLAVE' ];

  public function getTable() : string { return 'domains'; }
  public function getIdField() : string { return 'id'; }

  public function validateType($type) {
    return in_array($type, self::TYPES);
  }

  public function validateName($name) {
    return preg_match('/\b((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b/', $name);
  }

  public function getRecords($refresh = false) {
    if (!isset($this->records) || $refresh) {
      $this->records = $this->_em->findAll(Record::class, $this->id, 'domain_id');

      foreach ($this->records as $key => $record)
        if ($record->getType() == 'SOA') {
          $this->soa = $record->castAs(SOA::class);
          unset($this->records[$key]);
        }
    }
    return $this->records;
  }

  public function addRecord(Record $record) {
    $this->records[] = $record;
    if ($this->id) {
      $record->setDomain_id($this->id);
    }
  }

  public function getSOARecord(): ?SOA {
    if (!isset($this->soa)) {
      $this->getRecords();
    }
    if (!isset($this->soa)) {
      $this->soa = new SOA();
      $this->soa->setDomain_id($this->id);
    }
    return $this->soa;
  }

  public function save() {
    parent::save();
    if (isset($this->records)) {
      foreach ($this->records as $record)
        $record->save();
    }
    if (isset($this->soa))
      $this->soa->save();
  }

}
