<?php

declare(strict_types=1);

namespace PDNSAdmin\Model;

class User extends Entity {

  protected int $id;
  protected string $username;
  protected string $password;
  protected ?string $firstname;
  protected ?string $lastname;
  protected bool $is_admin;

  public function getTable() : string { return 'users'; }
  public function getIdField() : string { return 'id'; }

  public function setId($id) {
    if (isset($this->id))
      throw new \Exception("User ID already set", 1);
    $this->id = (int)$id;
  }

  public function setPassword(string $password) {
    $this->password = \password_hash($password, \PASSWORD_BCRYPT);
    $this->_hasChanged = true;
  }

  public function verifyPassword(string $password) {
    return \password_verify($password, $this->password);
  }

  public function getFullName() {
    if (!isset($this->firstname) && !isset($this->lastname))
      return $this->username;
    return ($this->firstname ?? ''). " " . ($this->lastname ?? '');
  }

  public function setAdmin($admin) {
    $this->is_admin = $admin ? true : false;
  }

  public function isAdmin() {
    if (isset($this->is_admin) && $this->is_admin)
      return true;
    return false;
  }

}
