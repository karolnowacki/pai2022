<?php

declare(strict_types=1);

namespace PDNSAdmin;

use Psr\Http\Message\ResponseInterface;

class View {

  protected string $templatePath;
  protected array $attributes;
  protected string $layout;
  protected array $notification = [];
  protected string $active = '';

  public function __construct($path = '.', $layout = 'index.phtml') {
    $this->layout = $layout;
    $this->templatePath = $path;
  }

  public function setLayout(string $layout) {
    $this->layout = $layout;
  }

  public function setAttribute(string $attr, $value) {
    $this->attributes[$attr] = $value;
  }

  public function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface {
    $output = $this->fetch($template, $data, true);
    $response->getBody()->write($output);
    return $response;
  }

  public function fetch(string $template, array $data = [], bool $useLayout = false): string {
    $content = $this->fetchTemplate($template, $data);
    if ($useLayout) {
      $data['content'] = $content;
      $content = $this->fetchTemplate($this->layout, $data);
    }
    return $content;
  }

  public function fetchTemplate(string $template, array $data = []): string {
    $data = array_merge($this->attributes, $data);

    ob_start();
    extract($data);
    include $this->templatePath . $template;
    $content = ob_get_clean();

    return $content;
  }

  public function html($str) {
    return htmlentities(strval($str), ENT_QUOTES | ENT_IGNORE, "UTF-8");
  }

  private function printIfError($error, $string) {
    if (isset($this->attributes['errors'][$error]))
      echo $string;
  }

  private function printError($error, $tag = 'div', $class = 'error') {
    if (isset($this->attributes['errors'][$error]))
      echo "<$tag class=\"$class\">" . $this->html($this->attributes['errors'][$error]). "</$tag>";
  }

  public function addNotification(string $msg, string $class = 'info') {
    $this->notification[] = ['msg' => $msg, 'class' => $class ];
  }

  public function setActive(string $name) {
    $this->active = $name;
  }

  public function isActive(string $name) {
    return $name == $this->active;
  }

}
