<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

use Psr\Container\ContainerInterface;

abstract class Command {

  protected $container;

  public function __construct(?ContainerInterface $container = null) {
    $this->container = $container;
  }

  abstract function run(array $args) : int;
  abstract function help() : string;
  abstract function description() : string;

}
