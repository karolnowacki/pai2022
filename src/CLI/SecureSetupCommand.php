<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

use PDNSAdmin\Services\EntityManager;

class SecureSetupCommand extends Command {

  public function run(array $args) : int {

    echo "SecureSetupCommand: ";

    if (!($configFile = realpath(__DIR__ . '/../../config.php'))) {
      echo "Config not found.\n";
      exit(1);
    }
    if (!is_writable($configFile)) {
      echo "Cannot write to config file.\n";
      exit(2);
    }

    $config = [ 'settings' => $this->container->get('settings') ];

    if (!$config['settings']['allow-cli-config-change']) {
      echo "Config change not allowed.\n";
      exit(3);
    }

    $config['settings']['auth']['secret'] = $this->randomString(60);

    $content = file_get_contents($configFile);
    $content = preg_replace('/return.*$/s', 'return ', $content);
    $content .= var_export($config, true);
    $content .= ';'.PHP_EOL;

    file_put_contents($configFile, $content);

    echo "Done.\n";
    return 0;
	}

  private function randomString($len) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $key = '';
    $max = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i=0; $i<$len; ++$i)
      $key .= $alphabet[rand(0, $max)];
    return $key;
  }

	public function description() : string { return "Generate and replace secret keys"; }
	public function help() : string { return "Generate and replace secret keys"; }

}
