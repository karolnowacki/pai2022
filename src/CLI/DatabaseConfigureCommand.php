<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

use PDNSAdmin\Services\EntityManager;

class DatabaseConfigureCommand extends Command {

  public function run(array $args) : int {

    echo "DatabaseConfigureCommand: ";

    if (count($args) < 1) {
      echo $this->help();
      return 1;
    }

    if (!($configFile = realpath(__DIR__ . '/../../config.php'))) {
      echo "Config not found.\n";
      exit(1);
    }
    if (!is_writable($configFile)) {
      echo "Cannot write to config file.\n";
      exit(2);
    }

    $config = [ 'settings' => $this->container->get('settings') ];

    if (!$config['settings']['allow-cli-config-change']) {
      echo "Config change not allowed.\n";
      exit(3);
    }

    $dsn = array_shift($args);
    $username = array_shift($args) ?? NULL;
    $password = array_shift($args) ?? NULL;

    try {
      $dbh = new \PDO($dsn, $username, $password, [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ]);
    } catch (\PDOException $e) {
      echo $e->getMessage()."\n";
      exit(3);
    }



    $config['settings']['database']['dsn'] = $dsn;
    $config['settings']['database']['username'] = $username;
    $config['settings']['database']['password'] = $password;

    $content = file_get_contents($configFile);
    $content = preg_replace('/return.*$/s', 'return ', $content);
    $content .= var_export($config, true);
    $content .= ';'.PHP_EOL;

    file_put_contents($configFile, $content);

    echo "Done.\n";

		return 0;
	}

	public function description() : string { return "Configure database"; }
	public function help() : string { return "Usage:\n\tsetup::database dsn [username] [password]\n\nValues in [] are optional.\n"; }

}
