<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

class HelpCommand extends Command {

  public function run(array $args) : int {
    print("Available commands:\n");
		foreach ($this->container->get('commands') as $command => $class) {
			print("\t$command\t - ".(new $class)->description()."\n");
		}
		return 0;
	}

	public function description() : string { return "Display help message"; }
	public function help() : string { return "Display help message"; }

}
