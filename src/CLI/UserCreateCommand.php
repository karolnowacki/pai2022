<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Model\User;

class UserCreateCommand extends Command {

  public function run(array $args) : int {

    echo "UserCreateCommand: ";

    if (count($args) < 2) {
      echo $this->help();
      return 1;
    }

    $user = new User();
    $user->setUsername(array_shift($args));
    $user->setPassword(array_shift($args));

    if ($arg = array_shift($args)) {
      if ($arg == 'admin') {
        $user->setAdmin(true);
      }
    }
    if ($arg = array_shift($args)) {
      $user->setFirstName($arg);
    }
    if ($arg = array_shift($args)) {
      $user->setLastName($arg);
    }

    try {
      $this->container->get(EntityManager::class)->persist($user);
      printf("User %s created with id %d\n", $user->getUsername(), $user->getId());
    } catch (\Throwable $e) {
      throw $e;
    }
		return 0;
	}

	public function description() : string { return "Create new user"; }
	public function help() : string { return "Usage:\n\tuser::create username password [admin] [FirstName] [Lastname]\n\nValues in [] are optional.\n"; }

}
