<?php

declare(strict_types=1);

namespace PDNSAdmin\CLI;

use PDNSAdmin\Services\EntityManager;

class DatabaseInitCommand extends Command {

  public function run(array $args) : int {

    echo "DatabaseInitCommand: ";

    $db = $this->container->get(EntityManager::class)->db();
    $driver = $db->getAttribute(\PDO::ATTR_DRIVER_NAME);
    print("Driver: $driver\n");
    $path = __DIR__ . '/../Model/schema_' . $driver . '.sql';
    if (!file_exists($path)) {
      print("Schema file ($path) not exists.");
    }
    print("Schema: ".realpath($path)."\n");

    $db->beginTransaction();
    $counter = 0;
    try {
       $db->exec(file_get_contents($path));
       $db->commit();
       print("Done.\n");
    } catch (\Throwable $e) {
      $db->rollBack();
      throw $e;
    }
		return 0;
	}

	public function description() : string { return "Initalize database schema"; }
	public function help() : string { return "Initalize database schema"; }

}
