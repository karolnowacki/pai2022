<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

class UriFactory implements UriFactoryInterface {

  public function createUri(string $uri = ''): UriInterface {
    $parts = parse_url($uri);

    if ($parts === false) {
        throw new InvalidArgumentException('URI cannot be parsed');
    }

    return new Uri(
      $parts['scheme'] ?? '',
      $parts['host'] ?? '',
      $parts['port'] ?? null,
      $parts['path'] ?? '',
      $parts['query'] ?? '',
      $parts['fragment'] ?? '',
      $parts['user'] ?? '',
      $parts['pass'] ?? '');
  }

  public function createFromGlobals(array $globals): Uri {
    $uri = new Uri();

    $https = $globals['HTTPS'] ?? false;
    $uri = $uri->withScheme($scheme = !$https || $https === 'off' ? 'http' : 'https');

    $host = '';
    if (isset($globals['HTTP_HOST']))
      $host = $globals['HTTP_HOST'];
    elseif (isset($globals['SERVER_NAME']))
      $host = $globals['SERVER_NAME'];

    $uri = $uri->withHost($host);

    $port = 80;
    if (!empty($globals['SERVER_PORT']))
      $port = (int)$globals['SERVER_PORT'];
    elseif ($scheme === 'https') $port = 443;

    $uri = $uri->withPort($port)->withQuery($globals['QUERY_STRING'] ?? '');

    $requestUri = '';
    if (isset($globals['REQUEST_URI'])) {
      $uriFragments = explode('?', $globals['REQUEST_URI']);
      $requestUri = $uriFragments[0];
    }

    $uri = $uri->withPath($requestUri);

    return $uri;

  }

}
