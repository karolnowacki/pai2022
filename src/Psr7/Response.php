<?php
declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class Response extends Message implements ResponseInterface {

  private $statusCode;
  private $reasonPhrase = '';

  public function getStatusCode(): int {
      return $this->statusCode;
  }

  public function getReasonPhrase(): string {
      return $this->reasonPhrase;
  }

  public function withStatus($code, $reasonPhrase = ''): self {
      $new = clone $this;
      $new->statusCode = $code;
      $new->reasonPhrase = $reasonPhrase;
      return $new;
  }

  public function withJson($payload): self {
    $new = $this->withHeader('Content-Type', 'application/json')
      ->withBody(Stream::create(json_encode($payload)));
    return $new;
  }
}
