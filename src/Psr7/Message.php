<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

abstract class Message implements MessageInterface {

  protected $headers = [];
  protected $stream = null;
  protected $serverParams = [];
  protected $cookies = [];
  protected $attributes = [];

  public function getProtocolVersion(): string {
    return $this->protocolVersion;
  }

  public function withProtocolVersion($version) {
    $clone = clone $this;
    $clone->protocolVersion = $version;
    return $clone;
  }

  //TODO: straszny bajzel z tymi headerami - do poprawki
  public function getHeaders(): array {
    return $this->headers;
  }

  public function hasHeader($header): bool {
    return isset($this->headers[$header]);
  }

  public function getHeader($header): array {
    return isset($this->headers[$header]) ? [ $this->headers[$header] ] : [];
  }

  public function getHeaderLine($header): string {
    return \implode(', ', $this->getHeader($header));
  }

  public function withHeader($header, $value): self {
    $clone = clone $this;
    $clone->headers[$header] = $value;
    return $clone;
  }

  public function withoutHeader($header): self{
    if (!isset($this->headers[$header]))
        return $this;
    $clone = clone $this;
    unset($clone->headers[$header]);
    return $clone;
  }

  public function withAddedHeader($header, $value): self {
    $clone = clone $this;
    $clone->setHeaders([$header => $value]);
    return $clone;
  }

  public function getBody(): StreamInterface {
    if (null === $this->stream)
      $this->stream = Stream::create('');
    return $this->stream;
  }

  public function withBody(StreamInterface $body): self {
    if ($body === $this->stream) {
        return $this;
    }
    $clone = clone $this;
    $clone->stream = $body;
    return $clone;
  }

  public function getServerParams(): array {
    return $this->serverParams;
  }

  public function getCookieParams(): array {
    return $this->cookies;
  }

  public function withCookieParams(array $cookies) {
    $clone = clone $this;
    $clone->cookies = $cookies;
    return $clone;
  }

  public function getQueryParams(): array {
    $queryParams = [];
    parse_str($this->uri->getQuery(), $queryParams);
    return $queryParams;
  }

  public function withQueryParams(array $query) {
    $clone = clone $this;
    $clone->uri = $clone->uri->withQuery(http_build_query($query));
    return $clone;
  }

  public function getUploadedFiles(): array {
    throw RuntimeException("Not implemented");
  }

  public function withUploadedFiles(array $uploadedFiles) {
    throw RuntimeException("Not implemented");
  }

  public function getAttributes(): array {
    return $this->attributes;
  }

  public function getAttribute($name, $default = null) {
    return $this->attributes[$name] ?? $default;
  }

  public function withAttribute($name, $value) {
    $clone = clone $this;
    $clone->attributes[$name] = $value;
    return $clone;
  }

  public function withoutAttribute($name) {
    $clone = clone $this;
    unset($clone->attributes[$name]);
    return $clone;
  }


}
