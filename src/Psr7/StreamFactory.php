<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

class StreamFactory implements StreamFactoryInterface {

  public function createStream(string $content = ''): StreamInterface {
    $resource = fopen('php://temp', 'rw+');
    if (!is_resource($resource)) {
      throw new \RuntimeException('Could not open temp file');
    }
    fwrite($resource, $content);
    rewind($resource);

    return $this->createStreamFromResource($resource);
  }

  public function createStreamFromFile(string $filename, string $mode = 'r', StreamInterface $cache = null): StreamInterface {
    $resource = fopen($filename, $mode);
    if (!is_resource($resource)) {
      throw new \RuntimeException('Could not open file $filename');
    }
    return new Stream($resource, $cache);
  }

  public function createStreamFromResource($resource, StreamInterface $cache = null): StreamInterface {
    if (!is_resource($resource)) {
      throw new \InvalidArgumentException('this is not a resource');
    }
    return new Stream($resource, $cache);
  }

}
