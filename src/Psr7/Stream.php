<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\StreamInterface;

class Stream implements StreamInterface {

  private $stream = null;

  public function __construct($stream, ?StreamInterface $cache = null) {
    $this->attach($stream);
  }

  public function __toString(): string  {
    if (!$this->stream)
      return '';

    if ($this->isSeekable()) {
        $this->rewind();
    }

    return $this->getContents();
  }

  protected function attach($stream): void {
    if (!is_resource($stream)) {
        throw new \InvalidArgumentException('this is not a resource');
    }
    if ($this->stream) {
        $this->detach();
    }
    $this->stream = $stream;
  }

  public function detach() {
    $stream = $this->stream;
    $this->stream = null;
    return $stream;
  }

  public function close(): void {
    if ($this->stream)
      fclose($this->stream);
    $this->detach();
  }

  public function getSize(): ?int {
    return fstat($this->stream)['size'];
  }

  public function tell(): int {
    return ftell($this->stream);
  }

  public function eof(): bool {
    return feof($this->stream);
  }

  public function getMetadata($key = null) {
    if (!$this->stream)
      return null;

    $meta = stream_get_meta_data($this->stream);
    if (!$key)
        return $meta;

    return $meta[$key] ?? null;
  }

  public function isSeekable(): bool {
    return $this->getMetadata('seekable');
  }

  public function seek($offset, $whence = SEEK_SET): void {
    fseek($this->stream, $offset, $whence);
  }

  public function rewind(): void {
    rewind($this->stream);
  }

  public function isWritable(): bool {
    $mode = $this->getMetadata('mode');
    if (is_string($mode) && (strstr($mode, 'w') !== false || strstr($mode, '+') !== false))
      return true;
    return false;
  }

  public function isReadable(): bool {
    $mode = $this->getMetadata('mode');
    if (is_string($mode) && (strstr($mode, 'r') !== false || strstr($mode, '+') !== false))
      return true;
    return false;
  }

  public function read($length): string {
    return fread($this->stream, $length);
  }

  public function write($string): int {
    return fwrite($this->stream, $string);
  }

  public function getContents(): string {
    return stream_get_contents($this->stream);
  }

  public static function create($body = ''): StreamInterface {
    $resource = fopen('php://temp', 'rw+');
    fwrite($resource, $body);
    return new self($resource);
  }

}
