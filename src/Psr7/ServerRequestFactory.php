<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServerRequestFactory implements ServerRequestFactoryInterface {

  protected StreamFactoryInterface $streamFactory;
  protected UriFactoryInterface $uriFactory;

  public function __construct() {
    $this->streamFactory = new StreamFactory();
    $this->uriFactory = new UriFactory();
  }

  public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface {
      if (is_string($uri)) {
          $uri = $this->uriFactory->createUri($uri);
      }

      $body = $this->streamFactory->createStream();

      return new Request($method, $uri, $headers, $cookies, $serverParams, $body);
  }

  public static function createFromGlobals(): Request {

    $method = $_SERVER['REQUEST_METHOD'] ?? 'GET';
    $uri = (new UriFactory())->createFromGlobals($_SERVER);

    $body = (new StreamFactory())->createStreamFromFile('php://input', 'r');

    $headers = [];
    $cookies = [];

    if (function_exists('getallheaders')) {
      //function not exists in CLI
      $headers = getallheaders();
      $cookies = $_COOKIE;
    }

    $request = new Request($method, $uri, $headers, $cookies, $_SERVER, $body);

    return $request;
  }
}
