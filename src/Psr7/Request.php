<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use PDNSAdmin\Interfaces\ParseBodyStrategyInterface;

class Request extends Message implements ServerRequestInterface {

  protected string $protocolVersion = '1.1';

  protected $uri;
  protected $method;

  protected $parsedBody;

  public function __construct($method, $uri, $headers, $cookies, $serverParams, $stream) {
    $this->method = $method;
    $this->uri = $uri;
    $this->headers = $headers;
    $this->cookies = $cookies;
    $this->serverParams = $serverParams;
    $this->stream = $stream;
  }

  public function getRequestTarget(): string {
    throw RuntimeException("Not implemented");
  }

  public function withRequestTarget($requestTarget) {
    throw RuntimeException("Not implemented");
  }

  public function getMethod(): string {
    return $this->method;
  }

  public function withMethod($method) {
    $clone = clone $this;
    $clone->method = $method;
    return $clone;
  }

  public function getUri(): UriInterface {
    return $this->uri;
  }

  public function withUri(UriInterface $uri, $preserveHost = false) {
    $clone = clone $this;
    $clone->uri = $uri;

    if (!$preserveHost && $uri->getHost() !== '') {
        $clone->setHeader('Host', $uri->getHost());
        return $clone;
    }

    if (($uri->getHost() !== '' && !$this->hasHeader('Host') || $this->getHeaderLine('Host') === '')) {
        $clone->setHeader('Host', $uri->getHost());
        return $clone;
    }

    return $clone;
  }

  public function getParsedBody() {
    return $this->parsedBody;
  }

  public function withParsedBody($data) {
    $clone = clone $this;
    $clone->parsedBody = $data;
    return $clone;
  }

}
