<?php

declare(strict_types=1);

namespace PDNSAdmin\Psr7;

use Psr\Http\Message\UriInterface;

class Uri implements UriInterface {

  private $scheme = '';
  private $host = '';
  private $port = null;
  private $path = '';
  private $query = '';
  private $fragment = '';
  private $user = '';
  private $pass = '';

  public function __construct(string $uri = '') {
    if ('' === $uri)
      return;

    if (false === $parts = \parse_url($uri)) {
      throw new \InvalidArgumentException("URI cannot be parsed");
    }

    $this->scheme = $parts['scheme'] ?? '';
    $this->host = $parts['host'] ?? '';
    $this->port = (int)$parts['port'] ?? null;
    $this->path = $parts['path'] ?? '';
    $this->query = $parts['query'] ?? '';
    $this->fragment = $parts['fragment'] ?? '';
    $this->user = $parts['user'] ?? '';
    $this->pass = $parts['pass'] ?? '';

  }

  public function getScheme(): string {
    return $this->scheme;
  }

  public function withScheme($scheme) {
    $clone = clone $this;
    $clone->scheme = $scheme;
    return $clone;
  }

  public function getAuthority(): string {
    $userInfo = $this->getUserInfo();
    $host = $this->getHost();
    $port = $this->getPort();

    return ($userInfo !== '' ? $userInfo . '@' : '') . $host . ($port !== null ? ':' . $port : '');
  }

  public function getUserInfo(): string {
      $info = $this->user;
      if ($this->pass !== '') {
          $info .= ':' . $this->pass;
      }
      return $info;
  }

  public function withUserInfo($user, $password = null) {
    $clone = clone $this;
    $clone->user = $user;

    $clone->password = '';
    if ($clone->user !== '') {
        $clone->password = $password;
    }
    return $clone;
  }

  public function getHost(): string {
    return $this->host;
  }

  public function withHost($host) {
    $clone = clone $this;
    $clone->host = $host;
    return $clone;
  }

  public function getPort(): ?int {
    return $this->port;
  }

  public function withPort($port) {
    $clone = clone $this;
    $clone->port = $port;
    return $clone;
  }

  public function getPath(): string {
    return $this->path;
  }

  public function withPath($path) {
    $clone = clone $this;
    $clone->path = $path;
    return $clone;
  }

  public function getQuery(): string  {
    return $this->query;
  }

  public function withQuery($query) {
    $query = ltrim($query, '?');
    $clone = clone $this;
    $clone->query = $query;
    return $clone;
  }

  public function getFragment(): string {
    return $this->fragment;
  }

  public function withFragment($fragment) {
    $clone = clone $this;
    $clone->fragment = $fragment;
    return $clone;
  }

  public function __toString(): string  {
    $uri = '';
    if ('' !== $this->scheme) {
        $uri .= $this->scheme . ':';
    }

    $auth = $this->getAuthority();

    if ('' != $auth) {
        $uri .= '//' . $auth;
    }

    if ('' !== $this->path) {
        if ('/' !== $this->path[0]) {
            if ('' != $auth) {
                $path = '/' . $path;
            }
        } elseif (isset($path[1]) && '/' === $path[1]) {
            if ('' === $auth) {
                $path = '/' . \ltrim($path, '/');
            }
        }
        $uri .= $path;
    }

    if ('' !== $query) {
        $uri .= '?' . $query;
    }

    if ('' !== $fragment) {
        $uri .= '#' . $fragment;
    }

    return $uri;
  }

}
