<?php

declare(strict_types=1);

namespace PDNSAdmin;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use PDNSAdmin\Interfaces\RouterInterface;
use PDNSAdmin\Routing\Router;
use PDNSAdmin\Routing\RouteRunner;
use PDNSAdmin\Psr7\ServerRequestFactory;
use PDNSAdmin\View;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;

class App implements RequestHandlerInterface {

  protected Router $router;
  protected MiddlewareDispatcher $middlewareDispatcher;
  protected View $view;
  protected ContainerInterface $container;

  public function __construct(?ContainerInterface $container = null) {
    $this->container = $container;
    $this->router = new Router($container);
    $this->middlewareDispatcher = new MiddlewareDispatcher($this->router, $container);
  }

  public function add($middleware) {
    if (!($middleware instanceof MiddlewareInterface))
      $middleware = new $middleware($this->container);
    // zwraca middlewareDispatcher
    return $this->middlewareDispatcher->add($middleware);
  }

  public function handle(ServerRequestInterface $request): ResponseInterface {
      return $this->middlewareDispatcher->handle($request);
  }

  public function run(?ServerRequestInterface $request = null): void {
    if (!$request) {
        $request = ServerRequestFactory::createFromGlobals();
    }

    $response = $this->handle($request);
    $responseEmitter = new ResponseEmitter();
    $responseEmitter->emit($response);
  }

  public function get(string $path, $handle) {
    return $this->router->get($path, $handle);
  }

  public function post(string $path, $handle) {
    return $this->router->post($path, $handle);
  }

}
