<?php

declare(strict_types=1);

namespace PDNSAdmin\Middleware;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Psr7\Response;
use Psr\Http\Message\StreamInterface;

class BodyParseMiddleware implements MiddlewareInterface {

  protected ContainerInterface $container;

  public function __construct(ContainerInterface $container = null) {
    $this->container = $container;
  }

  public function process(ServerRequestInterface $request, RequestHandlerInterface $next) : ResponseInterface {
    if ($request->hasHeader('Content-Type')) {
      if ($request->getHeader('Content-Type')[0] == 'application/x-www-form-urlencoded') {
        $request = $request->withParsedBody($this->parserUrlencoded($request->getBody()));
      }
    }
    return $next($request);
  }

  private function parserUrlencoded(StreamInterface $stream) {
    parse_str(strval($stream), $data);
    return $data;
  }


}
