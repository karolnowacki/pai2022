<?php

declare(strict_types=1);

namespace PDNSAdmin\Middleware;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Psr7\Response;
use PDNSAdmin\Services\AuthService;
use PDNSAdmin\Services\EntityManager;
use PDNSAdmin\Model\User;

class AuthMiddleware implements MiddlewareInterface {

  protected ContainerInterface $container;
  protected array $options = [
    'allow' => [],
    'redirect' => false
  ];

  public function __construct(ContainerInterface $container = null, ?array $options = []) {
    $this->container = $container;
    $this->options = array_merge($this->options, $options);
  }

  public function process(ServerRequestInterface $request, RequestHandlerInterface $next) : ResponseInterface {
    if (in_array($request->getUri()->getPath(), $this->options['allow']))
      return $next($request);

    if ($token = $this->getToken($request)) {
      if ($token = $this->container->get(AuthService::class)->validateToken($token)) {
        if ($user = $this->container->get(EntityManager::class)->findOne(User::class, $token->username, 'username')) {
          $this->container->set('user', $user);
          return $next($request);
        }
      }
    }
    if ($this->options['redirect'])
      return (new Response())->withProtocolVersion('1.1')->withStatus(302)->withHeader('Location', $this->options['redirect']);
    return (new Response())->withProtocolVersion('1.1')->withStatus(403, "Not authenticated");
  }

  private function getToken(ServerRequestInterface $request) {
    return $request->getCookieParams()['token'] ?? null;
  }

}
