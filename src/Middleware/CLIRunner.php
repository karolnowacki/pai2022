<?php

declare(strict_types=1);

namespace PDNSAdmin\Middleware;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;

class CLIRunner implements MiddlewareInterface {

  protected ContainerInterface $container;

  public function __construct(?ContainerInterface $container = null) {
    $this->container = $container;
  }

  public function process(ServerRequestInterface $request, RequestHandlerInterface $next) : ResponseInterface {
    if (PHP_SAPI !== 'cli') {
      return $next($request);
    }
    //TODO: Tutaj trochę zabijamy koncepcje PSR-7 ale będzie szybciej
    global $argv;
    $script = $argv[0];
    $command = isset($argv[1]) ? $argv[1] : "help";
    $args = array_slice($argv, 2);
    $possible_commands = $this->container->get('commands');
    if (!array_key_exists($command, $possible_commands)) {
      printf("Command %s not found\n", $command);
      $command = 'help';
    }
    $class = $possible_commands[$command];
    $task = new $class($this->container);
    if (in_array("--help", $args)) {
      echo $task->help()."\n";
      exit(0);
    }
    exit($task->run($args));
    //TODO: Ordynarne wyjści przed wykonaniem routingu. Można by to zrobić ładniej ale to już trochę overkill
  }

}
