<?php

declare(strict_types=1);

namespace PDNSAdmin;

use Psr\Http\Message\ResponseInterface;

class ResponseEmitter {

  const BUFF_SIZE = 4096;

  public function emit(ResponseInterface $response): void {
    if (headers_sent() === false) {
      foreach ($response->getHeaders() as $name => $value) {
          header($name . ": " .$value);
      }
      foreach ($response->getCookieParams() as $name => $value) {
          setcookie($name, $value);
      }
      $status = sprintf('HTTP/%s %s %s',
        $response->getProtocolVersion(),
        $response->getStatusCode(),
        $response->getReasonPhrase()
      );
      header($status, true, $response->getStatusCode());
    }
    $body = $response->getBody();
    $body->rewind();
    while (!$body->eof()) {
      echo $body->read(self::BUFF_SIZE);
    }
  }

}
