<?php

declare(strict_types=1);

namespace PDNSAdmin\Routing;

use PDNSAdmin\Interfaces\RouteInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDNSAdmin\Middleware\RoutingMiddleware;
use PDNSAdmin\Controllers\TestController;
use Psr\Container\ContainerInterface;
use PDNSAdmin\Psr7\Response;

class Router implements RequestHandlerInterface {

  protected ContainerInterface $container;

  protected $routes = [];

  public function __construct(?ContainerInterface $container = null) {
    $this->container = $container;
  }

  public function get(string $path, $handle) {
    $this->routes[] = new Route(["GET"], $path, $handle, $this->container);
  }

  public function post(string $path, $handle) {
    $this->routes[] = new Route(["POST"], $path, $handle, $this->container);
  }

  public function handle(ServerRequestInterface $request): ResponseInterface {
    if ($route = $this->resolve($request)) {
      return $route->run($request);
    }
    return (new Response())->withProtocolVersion('1.1')->withStatus(404, "Not found");
  }

  public function resolve(ServerRequestInterface $request) {
    $found = false;
    foreach ($this->routes as $route) {
      if ($route->match($request)) {
        $found = $route;
        break;
      }
    }
    return $found;
  }

  public function __invoke(ServerRequestInterface $request) : ResponseInterface {
    return $this->handle($request);
  }

}
