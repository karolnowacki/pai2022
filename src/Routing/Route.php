<?php

declare(strict_types=1);

namespace PDNSAdmin\Routing;

use PDNSAdmin\Interfaces\RouteInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use PDNSAdmin\MiddlewareDispatcher;
use PDNSAdmin\Psr7\Response;
use Psr\Container\ContainerInterface;

class Route implements RequestHandlerInterface {

  protected $method = [];
  protected $pattern = '';
  protected $callable;
  protected $arguments = [];

  protected ContainerInterface $container;

  protected MiddlewareDispatcher $middlewareDispatcher;

  public function __construct(array $method, string $pattern, $callable, ?ContainerInterface $container = null) {
    $this->method = $method;
    $this->pattern = $pattern;
    $this->regex_pattern = '@^'.preg_replace('/{(\w+)}/', '(?P<\1>\w+)', $pattern).'$@';
    $this->callable = $callable;
    $this->middlewareDispatcher = new MiddlewareDispatcher($this);
    $this->container = $container;
  }

  public function handle(ServerRequestInterface $request): ResponseInterface {
    $response = (new Response())
      ->withProtocolVersion('1.1')
      ->withStatus(200);

    if ($this->container && is_array($this->callable) && $this->container->has($this->callable[0])) {
      $this->callable[0] = $this->container->get($this->callable[0]);
    }

    return ($this->callable)($request, $response, $this->arguments);
    $response->getBody()->write($this->callable);
    return $response;
  }

  public function run(ServerRequestInterface $request): ResponseInterface {
    return $this->middlewareDispatcher->handle($request);
  }

  public function match(ServerRequestInterface $request) {
    if (!in_array($request->getMethod(), $this->method))
      return false;
    if (preg_match($this->regex_pattern, $request->getUri()->getPath(), $this->arguments))
      return true;
    return false;
  }

}
